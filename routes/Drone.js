const DronesController = require('../controller/Drone');
const kafka = require('kafka-node');
var msg;
/*const consumer = new kafka.ConsumerGroup(
    {
      kafkaHost: 'localhost:9092', // Adresse et port du broker Kafka
      groupId: 'my-group', // Identifiant du groupe de consommateurs
      fromOffset: 'latest', // Démarrer à partir du dernier offset
    },
    ['drone'] // Liste des sujets à consommer
  );

  consumer.on('message', (message) => {
        
    // Traitement du message
    msg= JSON.parse(message.value);
  });*/

/**
* @openapi
*components:
*  schemas:
*    Drone:
*      type: object
*      properties:
*        uid:
*          type: string
*        etat:
*          type: string
*        type:
*          type: string
*        charge:
*          type: string
*        batterie:
*          type: number
*        latitude:
*          type: number
*        longitude:
*          type: number
*        station:
*          type: array
*          items:
*            type: string  # or 'ObjectId' if it's a reference
* 
*/

exports.routesConfig = function (server) {

   /**
     * @openapi
     * /Drones:
     *   post:
     *     tags:
     *       - Drone
     *     summary: Create a new drone
     *     requestBody:
     *       description: drone data to be created
     *       required: true
     *       content:
     *         application/json:
     *           schema:
     *             $ref: '#/components/schemas/Drone'
     *     responses:
     *       201:
     *         description: drone created successfully
     *       400:
     *         description: Bad request
     */
  
    server.post('/Drones', DronesController.insert);

   /**
 * @openapi
 * /Drone:
 *   get:
 *     tags:
 *       - Drone
 *     summary: Activate a mission
 *     parameters:
 *       - name: latitude
 *         in: query
 *         description: Latitude of the mission
 *         required: true
 *         schema:
 *           type: number
 *       - name: longitude
 *         in: query
 *         description: Longitude of the mission
 *         required: true
 *         schema:
 *           type: number
 *     responses:
 *       200:
 *         description: Successful activation of the mission
 *       404:
 *         description: Error
 */

server.get('/Drone', DronesController.listDrone);


     /**
     * @openapi
     * /Drones/{DroneId}:
     *   get:
     *     tags:
     *       - Drone
     *     summary: Get drone details by ID
     *     parameters:
     *       - name: DroneId
     *         in: path
     *         description: ID of the drone to retrieve details for
     *         required: true
     *         schema:
     *           type: string
     *     responses:
     *       200:
     *         description: Successful response with drone details
     *       404:
     *         description: drone not found
     */
    server.get('/Drones/:DroneId',DronesController.getById);


     
    /**
     * @openapi
     * /Drones:
     *   get:
     *     tags:
     *       - Drone
     *     summary: Get a list of all drones
     *     responses:
     *       200:
     *         description: Successful response with the list of drones
     */
    server.get('/Drones',DronesController.list);

    /**
     * @openapi
     * /Drones/Stations/{StationId}:
     *   get:
     *     tags:
     *       - Drone
     *     summary: Get drone list by station id
     *     parameters:
     *       - name: StationId
     *         in: path
     *         description: ID of the station to retrieve list of drone
     *         required: true
     *         schema:
     *           type: string
     *     responses:
     *       200:
     *         description: Successful response with drone details
     *       404:
     *         description: drone not found
     */
    server.get('/Drones/Stations/:StationId',DronesController.getByStationId);

    /**
     * @openapi
     * /Drones/{DroneId}:
     *   patch:
     *     tags:
     *       - Drone
     *     summary: patch drone details by ID
     *     parameters:
     *       - name: DroneId
     *         in: path
     *         description: ID of the drone to update
     *         required: true
     *         schema:
     *           type: string
     *     responses:
     *       200:
     *         description: drone is update
     *       404:
     *         description: drone not found
     */

    server.patch('/Drones/:DroneId',DronesController.patchById);
    
     /**
     * @openapi
     *  /Drones/{DroneId}:
     *   delete:
     *     tags:
     *       - Station
     *     summary: Delete drone by ID
     *     parameters:
     *       - name: DroneId
     *         in: path
     *         description: ID of the drone to delete
     *         required: true
     *         schema:
     *           type: string
     *     responses:
     *       204:
     *         description: drone deleted successfully
     *       404:
     *         description: drone not found
     */
    server.delete('/Drones/:DroneId',DronesController.removeById);

    
    server.get('/consume', (req, res) => {
    // Répondre avec les données consommées
    if (msg === undefined) {
      // La variable msg est indéfinie
      res.json([]); 
    } else {
      // La variable msg a une valeur définie
      res.json(msg); 
    }
       
  });
    
};