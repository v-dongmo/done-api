const StationsController = require('../controller/Station');

/**
 * @openapi
 * components:
 *   schemas:
 *     Station:
 *       type: object
 *       properties:
 *         lieu:
 *           type: string
 *         latitude:
 *           type: number
 *         longitude:
 *           type: number
 */

exports.routesConfig = function (server) {
   
  /**
     * @openapi
     * /Stations:
     *   post:
     *     tags:
     *       - Station
     *     summary: Create a new station
     *     requestBody:
     *       description: station data to be created
     *       required: true
     *       content:
     *         application/json:
     *           schema:
     *             $ref: '#/components/schemas/Station'
     *     responses:
     *       201:
     *         description: Station created successfully
     *       400:
     *         description: Bad request
     */
    server.post('/Stations', StationsController.insert);
    
    /**
     * @openapi
     * /Stations:
     *   get:
     *     tags:
     *       - Station
     *     summary: Get a list of all stations
     *     responses:
     *       200:
     *         description: Successful response with the list of stations
     */
    server.get('/Stations', StationsController.list);

    /**
     * @openapi
     * /Stations/{StationId}:
     *   get:
     *     tags:
     *       - Station
     *     summary: Get station details by ID
     *     parameters:
     *       - name: StationId
     *         in: path
     *         description: ID of the station to retrieve details for
     *         required: true
     *         schema:
     *           type: string
     *     responses:
     *       200:
     *         description: Successful response with station details
     *       404:
     *         description: Station not found
     */
    server.get('/Stations/:StationId', StationsController.getById);

    /**
     * @openapi
     * /Stations/{StationId}:
     *   patch:
     *     tags:
     *       - Station
     *     summary: Update station details by ID
     *     parameters:
     *       - name: StationId
     *         in: path
     *         description: ID of the station to update
     *         required: true
     *         schema:
     *           type: string
     *     responses:
     *       200:
     *         description: Station updated successfully
     *       400:
     *         description: Bad request
     *       404:
     *         description: Station not found
     */
    server.patch('/Stations/:StationId', StationsController.patchById);

    /**
     * @openapi
     * /Stations/{StationId}:
     *   delete:
     *     tags:
     *       - Station
     *     summary: Delete station by ID
     *     parameters:
     *       - name: StationId
     *         in: path
     *         description: ID of the station to delete
     *         required: true
     *         schema:
     *           type: string
     *     responses:
     *       204:
     *         description: Station deleted successfully
     *       404:
     *         description: Station not found
     */
    server.delete('/Stations/:StationId', StationsController.removeById);
};

