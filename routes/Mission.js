const MissionsController = require('../controller/Mission');


/**
 * @openapi
 * components:
 *   schemas:
 *     Mission:
 *       type: object
 *       properties:
 *         etat:
 *           type: string
 *         poids:
 *           type: string
 *         date:
 *           type: string
 *         type:
 *           type: string
 *         lieu:
 *           type: string
 *         surface:
 *           type: number
 *         position:
 *           type: object
 *         numero:
 *           type: string
 *         drone:
 *           type: array
 *           items:
 *             type: string  # or 'ObjectId' if it's a reference
 */

exports.routesConfig = function (server) {
    

    /**
     * @openapi
     * /Missions:
     *   post:
     *     tags:
     *       - Mission
     *     summary: Create a new mission
     *     requestBody:
     *       description: Mission data to be created
     *       required: true
     *       content:
     *         application/json:
     *           schema:
     *             $ref: '#/components/schemas/Mission'
     *     responses:
     *       201:
     *         description: Mission created successfully
     *       400:
     *         description: Bad request
     */

    server.post('/Missions', MissionsController.insert);
    
      /**
   * @openapi
   * /Missions:
   *   get:
   *     tags:
   *       - Mission
   *     summary: Get mission details 
   *     responses:
   *       200:
   *         description: Successful response with mission
   *       404:
   *         description: Mission not found
   */
    server.get('/Missions',MissionsController.list);


    /**
   * @openapi
   * /Missions/{MissionId}:
   *   get:
   *     tags:
   *       - Mission
   *     summary: Get mission details by ID
   *     parameters:
   *       - name: MissionId
   *         in: path
   *         description: ID of the mission to retrieve
   *         required: true
   *         schema:
   *           type: string
   *     responses:
   *       200:
   *         description: Successful response with mission
   *       404:
   *         description: Mission not found
   */
  server.get('/Missions/:MissionId', MissionsController.getById);

    /**
   * @openapi
   * /Missions/Drones/{DroneId}:
   *   get:
   *     tags:
   *       - Mission
   *     summary: Get missions by Drone ID
   *     parameters:
   *       - name: DroneId
   *         in: path
   *         description: ID of the drone to retrieve missions for
   *         required: true
   *         schema:
   *           type: string
   *     responses:
   *       200:
   *         description: Successful response with missions for the drone
   *       404:
   *         description: Drone not found or no missions for the drone
   */
  server.get('/Missions/Drones/:DroneId', MissionsController.getByDroneId);

  /**
   * @openapi
   * /Missions/{MissionId}:
   *   patch:
   *     tags:
   *       - Mission
   *     summary: Update mission details by ID
   *     parameters:
   *       - name: MissionId
   *         in: path
   *         description: ID of the mission to update
   *         required: true
   *         schema:
   *           type: string
   *     responses:
   *       200:
   *         description: Mission updated successfully
   *       400:
   *         description: Bad request
   *       404:
   *         description: Mission not found
   */
  server.patch('/Missions/:MissionId', MissionsController.patchById);

  /**
   * @openapi
   * /Missions/{MissionId}:
   *   delete:
   *     tags:
   *       - Mission
   *     summary: Delete mission by ID
   *     parameters:
   *       - name: MissionId
   *         in: path
   *         description: ID of the mission to delete
   *         required: true
   *         schema:
   *           type: string
   *     responses:
   *       204:
   *         description: Mission deleted successfully
   *       404:
   *         description: Mission not found
   */
  server.delete('/Missions/:MissionId', MissionsController.removeById);

};

