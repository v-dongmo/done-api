const MissionModel = require('../model/Mission');

exports.insert = (req, res) => {
    MissionModel.createMission(req.body)
        .then((result) => {
            res.status(201).send({id: result._id});
        }).catch(e=>{
            res.status(400).send({error: e.message});
        });
};

exports.list = (req , res) => {
  
    MissionModel.list()
        .then((result) => {
            res.status(200).send(result);
        }).catch(e=>{
            res.status(400).send({error: e.message});
        })
};



exports.getById = (req, res) => {
    MissionModel.findById(req.params.MissionId)
        .then((result) => {
            res.status(200).send(result);
        }).catch(e=>{
            res.status(400).send({error: e.message});
        });
};


exports.getByDroneId = (req, res) => {
    MissionModel.findByDrone(req.params.DroneId)
        .then((result) => {
            res.status(200).send(result);
        }).catch(e=>{
            res.status(400).send({error: e.message});
        });
};



exports.patchById = (req, res) => {
    MissionModel.patchMission(req.params.MissionId, req.body)
        .then((result) => {
            res.status(204).send({});
        }).catch(e=>{
            res.status(400).send({error: e.message});
        });
};

exports.removeById = (req, res) => {
    MissionModel.removeById(req.params.MissionId)
        .then((result)=>{
            res.status(204).send({});
        }).catch(e=>{
            res.status(400).send({error: e.message});
        });
};

