const DroneModel = require('../model/Drone');


exports.insert = (req, res) => {

    DroneModel.createDrone(req.body)
        .then((result) => {
            res.status(201).send({id: result._id});
        }).catch(e=>{
            res.status(400).send({error: e.message});
        });
};



exports.list = (req, res) => {

    DroneModel.list()
        .then((result) => {
            console.log(result);
            res.status(200).send(result);
        }).catch(e=>{
            res.status(400).send({error: e.message});
        })
};


exports.listDrone = (req, res) => {
    console.log('ok')
    mission = req.query.mission;
    console.log(mission);
    latitude= req.query.latitude;
    console.log(latitude);
    longitude = req.query.longitude;
    console.log(longitude);

    
    DroneModel.getDrone(latitude,longitude,mission)
            .then((result) => {
                if (result === null) {
                    res.status(200).json([]); // Renvoyer un tableau JSON vide
                  } else {
                    res.status(200).send(result); // Renvoyer le résultat
                  }
            } ).catch(e=> {
                res.status(400).send({error: e.message});
            } )
};


exports.getByStationId = (req, res) => {

    DroneModel.findByStation(req.params.StationId)
        .then((result) => {
            res.status(200).send(result);
        }).catch(e=>{
            res.status(400).send({error: e.message});
        });
};

exports.getById = (req, res) => {

    console.log("bonjour");
    DroneModel.findById(req.params.DroneId)
        .then((result) => {
            res.status(200).send(result);
        }).catch(e=>{
            res.status(400).send({error: e.message});
        });
};



exports.patchById = (req, res) => {
    DroneModel.patchDrone(req.params.DroneId, req.body)
        .then((result) => {
            res.status(204).send({});
        }).catch(e=>{
            res.status(400).send({error: e.message});
        });
};


exports.removeById = (req, res) => {

    DroneModel.removeById(req.params.DroneId)
        .then((result)=>{
            res.status(204).send({});
        }).catch(e=>{
            res.status(400).send({error: e.message});
        });
};

