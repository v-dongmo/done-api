const express = require('express');
const cors = require('cors');
const swaggerDocs = require('./swagger');
const app = express();
const server = require('http').createServer(app);
const path = require('path');

const mongoose = require('mongoose');
const dotenv = require('dotenv');

app.use(cors());
app.use(express.json());
mongoose.set('strictQuery', false);


app.use(function (req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
    res.header('Access-Control-Expose-Headers', 'Content-Length');
    res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Range');
    if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    } else {
        return next();
    }
});

const droneRouter = require('./routes/Drone');
const stationRouter = require('./routes/Station');
const missionRouter = require('./routes/Mission');


if(process.env.NODE_ENV !== 'production'){
    dotenv.config(); 
}
const PORT=process.env.PORT || 3000;
const CONNECTION = process.env.CONNECTION;


droneRouter.routesConfig(app);
stationRouter.routesConfig(app);
missionRouter.routesConfig(app);


const start = async() => {
    try {
        await  mongoose.connect(CONNECTION);
        server.listen(PORT, function () {
            console.log('app listening at port %s', PORT);
        });
        
    } catch (e) {

        console.log(e.message);
    }
     
}

app.use(express.static(__dirname));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'service.html'));
  });
  
start();
swaggerDocs(app, PORT); 

