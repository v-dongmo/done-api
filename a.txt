.container {
  width: 800px;
  margin: 0 auto;
  padding: 20px;
  border: 1px solid #ccc;
  border-radius: 10px;
  background-color: #f2f2f2;
  display: flex;
  align-items: flex-start;
}

.left-column {
  flex: 1;
  padding-right: 20px;
  border-right: 1px solid #ccc;
  text-align: ce;
}

.right-column {
  flex: 1;
  padding-left: 20px;
}

h2 {
  color: #333;
  text-align: center;
}

form {
  display: flex;
  flex-direction: column;
}

label {
  margin-bottom: 10px;
}

input[type="date"],
input[type="email"],
select {
  padding: 5px;
  border-radius: 5px;
  border: 1px solid #ccc;
}

input[type="submit"] {
  padding: 10px 20px;
  margin-top: 10px;
  border: none;
  border-radius: 5px;
  background-color: #4CAF50;
  color: white;
  cursor: pointer;
}