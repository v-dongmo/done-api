const Station = require('./Station');
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Mission = require('./Mission')
const DroneSchema = new Schema({
    uid : String,
    etat:String,
    type:String,
    charge:String,
    batterie : Number,
    latitude:Number,
    longitude:Number,
    station : String,
}
,
  { timestamps: true });


DroneSchema.virtual('id').get(function () {
    return this._id.toHexString();
});


// Ensure virtual fields are serialised.
DroneSchema.set('toJSON', {
    virtuals: true
});


/*DroneSchema.post('updateOne', function (result) {
  // Accédez aux informations de mise à jour
  const filter = result._update.$set.uid; // filtre pour l'identifiant unique
  const updatedDocument = result._update.$set; // document mis à jour
  console.log(updatedDocument);
  // Émettez les données mises à jour via le socket
  io.emit('server:drone', { drone: updatedDocument });
});
*/


DroneSchema.findById = function (cb) {
    return this.model('Drone').find({id: this.id}, cb);
};


const Drone = mongoose.model('Drone', DroneSchema);


exports.findById = (id) => {
    return Drone.find({_id: id}).populate('station')
        .then((result) => {
            delete result._id;
            delete result.__v;
            return result;
        });
};



function calculateDistance(lat1, lon1, lat2, lon2) {
  const R = 6371; // Rayon moyen de la Terre en kilomètres
  const dLat = toRadians(lat2 - lat1);
  const dLon = toRadians(lon2 - lon1);

  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(toRadians(lat1)) * Math.cos(toRadians(lat2)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2);

  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const distance = R * c; // Distance en kilomètres

  return distance;
}

function toRadians(degrees) {
  return degrees * (Math.PI / 180);
}

exports.getDrone = async (latitude,longitude,mission) => {
 
  console.log(latitude , longitude,mission);
  try {
    const drones = await Drone.find({
      etat: 'OFF',
      station: { $exists: true }
    })
      .sort({ batterie: -1 })
      .exec();

    let nearestDrone = null;
    let shortestDistance = Infinity;

    console.log(drones);

    drones.forEach(drone => {
      const distance = calculateDistance(latitude, longitude, drone.latitude, drone.longitude);
      if (distance < shortestDistance) {
        nearestDrone = drone;
        shortestDistance = distance;
      }
    });
    

    if (nearestDrone) {
      // Mettre à jour l'état du drone à "ON"
      await Drone.findByIdAndUpdate(nearestDrone._id, { etat: 'ON' });

      // Mettre à jour la mission en lui affectant l'id du drone
      await Mission.findByIdAndUpdate(mission, { drone: [nearestDrone._id] , etat:"EN COURS" });
    } else {
      console.log('Aucun drone disponible.');
    }
    console.log(nearestDrone);
    return nearestDrone;
  } catch (error) {
    console.error(error);
    return null;
  }
};





exports.findByStation = (station) => {
    return Drone.find({station: station})
                .sort({created_at:-1});
};


   
exports.createDrone = async (data) => {
    const station = await Station.findByLieu(data.station);
    console.log(station)
    if (!station) {
      throw new Error(`Station with id ${data.station} not found`);
    }
    
    const drone = new Drone({
      uid: data.uid,
      batterie:100,
      etat: "OFF",
      charge : data.charge,
      type: data.type,
      latitude : station.latitude,
      longitude : station.longitude,
      station: station.lieu
    });
    
    return drone.save();
  };  



exports.list = () => {
    return Drone.find();
};



exports.patchDrone = (uid, data) => {
    return Drone.findOneAndUpdate({
        uid: uid
    }, data);
};


exports.removeById = (id) => {
  return Drone.findByIdAndRemove(id)
      .then((result) => {
          if (result) {
              console.log(`drone with id ${id} has been removed`);
              return true;
          } else {
              console.log(`drone with id ${id} not found`);
              return false;
          }
      })
      .catch((error) => {
          console.log(error);
          throw new Error(`Failed to remove drone with id ${id}`);
      });
};
