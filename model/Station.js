const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const StationSchema = new Schema({
    lieu : String,
    latitude:Number,
    longitude:Number,
}
,
  { timestamps: true });

StationSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
StationSchema.set('toJSON', {
    virtuals: true
});

StationSchema.findById = function (cb) {
    return this.model('Station').find({id: this.id}, cb);
};

const Station = mongoose.model('Station', StationSchema);


exports.findById = (id) => {
    return Station.findById(id)
        .then((result) => {
            result = result.toJSON();
            delete result._id;
            delete result.__v;
            return result;
        });
};

exports.findByLieu = (lieu) => {
    return Station.find({ lieu })
        .then((results) => {
            return results.map(result => {
                result = result.toJSON();
                delete result._id;
                delete result.__v;
                return result;
            });
        });
};


exports.createStation = async (data) => {
  try {
    // Vérifiez si le lieu existe déjà dans la base de données
    const exists = await Station.exists({ lieu: data.lieu });

    // Si le lieu existe déjà, retournez une erreur
    if (exists) {
      throw new Error('Le lieu existe déjà dans la base de données.');
    }

    const station = new Station(data);
    return await station.save();
  } catch (error) {
    console.log(error);
    throw new Error('Une erreur s\'est produite lors de la création de la station.');
  }
};

exports.list = () => {
    return Station.find();
};

exports.patchStation = (id, data) => {
    return Station.findOneAndUpdate({
        _id: id
    }, data);
};


exports.removeById = (id) => {
    return Station.findByIdAndRemove(id)
        .then((result) => {
            if (result) {
                console.log(`Station with id ${id} has been removed`);
                return true;
            } else {
                console.log(`Station with id ${id} not found`);
                return false;
            }
        })
        .catch((error) => {
            console.log(error);
            throw new Error(`Failed to remove station with id ${id}`);
        });
};
