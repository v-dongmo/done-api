const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const MissionSchema = new Schema({
    etat:String,
    poids:String,
    date:String,
    type:String,
    lieu:String,
    surface:Number,
    position: {},
    numero:String,
    drone: [{ type: Schema.Types.ObjectId, ref: 'Drone'}],
}
);

MissionSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
MissionSchema.set('toJSON', {
    virtuals: true
});


const Mission = mongoose.model('Mission', MissionSchema);
 

exports.findByDrone = (drone) => {
    return Mission.find({drone: drone})
                .sort({created_at:-1});
};


exports.findByIdAndUpdate = (missionId, updateData) => {
    return Mission.findByIdAndUpdate(
        missionId, // ID de la mission que vous souhaitez mettre à jour
        updateData, // Les données à mettre à jour (par exemple, { drone: "nouvel_id_du_drone", etat: "EN COURS" })
        { new: true } // Option pour renvoyer la mission mise à jour
    );
};

exports.createMission = async (data) => {
    console.log(data)
    try {
  
      const mission  = new Mission ({
        etat:data.etat,
        poids:data.poids,
        date:data.date,
        surface:data.surface,
        lieu:data.lieu,
        type:data.type,
        numero:data.numero,
        position:data.position
      });
  
      const result = await mission.save();
      return result;
    } catch (error) {
      throw new Error(error.message);
    }
  };



exports.findById = (id) => {
    return Mission.findById(id).populate('drone')
        .then((result) => {
            result = result.toJSON();
            delete result._id;
            delete result.__v;
            return result;
        });
};


exports.findByDrone = (drone) => {
    return Mission.find({drone: drone});
};


exports.list = () => {
    return Mission.find().populate('drone');
};


exports.patchMission = (id, data) => {
    return Mission.findOneAndUpdate({
        _id: id
    }, data);
};


exports.removeById = (id) => {
    return Mission.findByIdAndRemove(id)
        .then((result) => {
            if (result) {
                console.log(`mission with id ${id} has been removed`);
                return true;
            } else {
                console.log(`mission with id ${id} not found`);
                return false;
            }
        })
        .catch((error) => {
            console.log(error);
            throw new Error(`Failed to mission station with id ${id}`);
        });
};
